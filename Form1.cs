﻿using CefSharp.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vlc.DotNet.Forms;

namespace VLCStream
{
    public partial class Form1 : Form
    {
        Image signature;
        bool clicked = false;
        Point previousPoint;
        System.Windows.Forms.Panel panelCha;
        ChromiumWebBrowser browser;
        System.Windows.Forms.Panel webUpload;
        private int camW = 0;
        private int camH = 0;
        private int formW = 0;
        private int formH = 0;
        private string urlVung = " ";
        private string urlSound = " ";
        private string urlCam = " ";

        public Form1()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.DoubleBuffer, true);
            InitializeComponent();
            this.Location = new Point(0, 0);
        }
        draw1 dr = null;
        private void Form1_Load(object sender, EventArgs e)
        {
            StreamReader reader = new StreamReader("config.txt");
            camW = int.Parse(reader.ReadLine().Trim().Split(new char[] { '#' })[0].Trim());
            camH = int.Parse(reader.ReadLine().Trim().Split(new char[] { '#' })[0].Trim());
            formW = int.Parse(reader.ReadLine().Trim().Split(new char[] { '#' })[0].Trim());
            formH = int.Parse(reader.ReadLine().Trim().Split(new char[] { '#' })[0].Trim());
            urlVung = reader.ReadLine().Trim().Split(new char[] { '#' })[0].Trim();
            urlSound = reader.ReadLine().Trim().Split(new char[] { '#' })[0].Trim();
            urlCam = reader.ReadLine().Trim().Split(new char[] { '#' })[0].Trim();
            reader.Close();
            //this.Paint += Form1_Paint;
            this.Size = new Size(formW, formH);
            this.MouseDown += Form1_MouseDown;
            this.MouseUp += Form1_MouseUp;
            this.MouseMove += Form1_MouseMove;
            this.MouseLeave += Form1_MouseLeave;
            this.FormClosing += Form1_FormClosing;

            this.vlcControl1.Location = new Point(0, 0);
            this.vlcControl1.Size = new Size(camW, camH);

            //this.snapShot.Visible = false;
            //this.snapShot.Enabled = false;

            webUpload = new System.Windows.Forms.Panel
            {
                Parent = this,
                Location = new Point(0, 0),
                Size = new Size(camW, camH)
            };
            browser = new ChromiumWebBrowser(urlSound);
            this.Controls.Add(webUpload);
            this.webUpload.Controls.Add(browser);
            this.webUpload.Visible = false;
            this.webUpload.Enabled = false;

            panelCha = new System.Windows.Forms.Panel
            {
                Parent = this,
                Size = new Size(camW, camH),
                Location = new Point(0, 0),
            };
            

            textBox1.Text = urlCam;
            vlcControl1.Play(urlCam);
        }
        void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Dispose signature after closing the form to avoid memory leak
        }

        void Form1_Paint(object sender, PaintEventArgs e)
        {
            if (signature != null)
                e.Graphics.DrawImage(signature, 0, 0);
        }

        void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            clicked = true;
            previousPoint = e.Location;

        }

        void Form1_MouseLeave(object sender, EventArgs e)
        {
            clicked = false;
        }

        void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            clicked = false;
        }

        void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (clicked)
            {
                if (signature == null)
                    signature = new Bitmap(this.Width, this.Height);
                using (Graphics g = Graphics.FromImage(signature))
                {
                    g.DrawLine(Pens.Black, previousPoint, e.Location);
                    previousPoint = e.Location;
                    this.Invalidate();
                }
            }
        }
        private string cap = "abc.png";
        private void capture_Click(object sender, EventArgs e)
        {
            try
            {
                dr = new draw1(this.Location.X, this.Location.Y);
                this.dr.Parent = this.panelCha;
                this.dr.Size = new Size(camW, camH);
                this.dr.Location = new Point(0, 0);
                this.dr.BackgroundImageLayout = ImageLayout.Stretch;
                this.panelCha.Controls.Add(dr);
                this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer, true);

                File.Delete("demo.txt");
                this.vlcControl1.TakeSnapshot(cap, 640, 480);
                FileStream fs = new FileStream(cap, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                fs.Seek(0, SeekOrigin.End);
                int count = (int)fs.Position;
                fs.Seek(0, SeekOrigin.Begin);
                byte[] b = br.ReadBytes(count);
                br.Close();
                fs.Close();
                MemoryStream ms = new MemoryStream(b, 0, count);
                ms.Position = 0;
                dr.BackgroundImage = Image.FromStream(ms);
                ms.Close();
                try
                {
                    File.Delete("abc.png");
                }
                catch
                {

                }
                this.panelCha.Visible = true;
                this.panelCha.Enabled = true;
                this.vlcControl1.Visible = false;
                this.vlcControl1.Enabled = false;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            

        }

        private void upload_Click(object sender, EventArgs e)
        {
            this.dr.Visible = false;
            this.dr.Enabled = false;
            this.vlcControl1.Visible = true;
            this.vlcControl1.Enabled = true;
            try
            {
                dr.Dispose();
            }
            catch
            {

            }
            dr = null;
            try
            {
                var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(urlVung);
                var postData = "";
                foreach (string line in File.ReadAllLines("demo.txt"))
                {
                    postData += line + ",";

                }
                var data = Encoding.ASCII.GetBytes(postData);

                request.Method = "POST";
                request.ContentType = "text/html; charset=UTF-8";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (System.Net.HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                if (responseString == "1")
                {
                    MessageBox.Show("thành công");
                }
                else
                {
                    MessageBox.Show("thất bại");
                }
            }
            catch
            {
                MessageBox.Show("Switch có vấn đề hoặc Server bị tắt");
            }
        }

        private void web_Click(object sender, EventArgs e)
        {
            this.dr.Visible = false;
            this.dr.Enabled = false;
            try
            {
                dr.Dispose();
            }
            catch
            {

            }
            dr = null;
            this.vlcControl1.Visible = false;
            this.vlcControl1.Enabled = false;
            this.webUpload.Visible = true;
            this.webUpload.Enabled = true;
            this.webUpload.BringToFront();
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            if (this.dr != null)
            {
                this.dr.Visible = false;
                this.dr.Enabled = false;
                try
                {
                    dr.Dispose();
                }
                catch
                {

                }
            }
            dr = null;
            this.panelCha.Visible = false;
            this.panelCha.Enabled = false;
            this.panelCha.Refresh();
            this.vlcControl1.Visible = true;
            this.vlcControl1.Enabled = true;
            
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
    public class draw1 : Panel
    {
        private Point _origin = Point.Empty;
        private Point _terminus = Point.Empty;
        private Boolean _draw = false;
        private List<Tuple<Point, Point>> _lines = new List<Tuple<Point, Point>>();
        private int parent_X, parent_Y;
        public draw1(int parentX, int parentY)
        {
            a.Width = 3f;
            Dock = DockStyle.Fill;
            DoubleBuffered = true;
            parent_X = parentX;
            parent_Y = parentY;
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {

            base.OnMouseDown(e);
            if (e.Button == MouseButtons.Left)
            {
                _draw = true;
                _origin = e.Location;
            }
            else
            {
                _draw = false;
                _origin = Point.Empty;
            }

            System.IO.StreamWriter writer = new System.IO.StreamWriter("demo.txt", true); //open the file for writing.
            writer.WriteLine("(" + (MousePosition.X - parent_X) + "," + (MousePosition.Y-parent_Y) + ")"); //write the current date to the file. change this with your date or something.
            writer.Close(); //remember to close the file again.
            writer.Dispose();
            _terminus = Point.Empty;
            Invalidate();
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            if (_draw && !_origin.IsEmpty && !_terminus.IsEmpty)
                _lines.Add(new Tuple<Point, Point>(_origin, _terminus));
            _draw = false;
            _origin = Point.Empty;
            _terminus = Point.Empty;
            Invalidate();
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (e.Button == MouseButtons.Left)
                _terminus = e.Location;
            Invalidate();
        }
        Pen a = new Pen(new SolidBrush(Color.Red));
        protected override void OnPaint(PaintEventArgs e)
        {
            foreach (var line in _lines)
                e.Graphics.DrawLine(a, line.Item1, line.Item2);
            if (!_origin.IsEmpty && !_terminus.IsEmpty)
                e.Graphics.DrawLine(a, _origin, _terminus);
        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x00000020; // WS_EX_TRANSPARENT
                return cp;
            }
        }
    }
}
