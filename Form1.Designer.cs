﻿
namespace VLCStream
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.vlcControl1 = new Vlc.DotNet.Forms.VlcControl();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.capture = new System.Windows.Forms.Button();
            this.upload = new System.Windows.Forms.Button();
            this.web = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.exit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.vlcControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // vlcControl1
            // 
            this.vlcControl1.BackColor = System.Drawing.Color.Black;
            this.vlcControl1.Location = new System.Drawing.Point(12, 12);
            this.vlcControl1.Name = "vlcControl1";
            this.vlcControl1.Size = new System.Drawing.Size(640, 480);
            this.vlcControl1.Spu = -1;
            this.vlcControl1.TabIndex = 0;
            this.vlcControl1.Text = "vlcControl1";
            this.vlcControl1.VlcLibDirectory = ((System.IO.DirectoryInfo)(resources.GetObject("vlcControl1.VlcLibDirectory")));
            this.vlcControl1.VlcMediaplayerOptions = null;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(142, 527);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(332, 20);
            this.textBox1.TabIndex = 4;
            // 
            // capture
            // 
            this.capture.Location = new System.Drawing.Point(142, 498);
            this.capture.Name = "capture";
            this.capture.Size = new System.Drawing.Size(75, 23);
            this.capture.TabIndex = 8;
            this.capture.Text = "Bắt đầu vẽ";
            this.capture.UseVisualStyleBackColor = true;
            this.capture.Click += new System.EventHandler(this.capture_Click);
            // 
            // upload
            // 
            this.upload.Location = new System.Drawing.Point(358, 553);
            this.upload.Name = "upload";
            this.upload.Size = new System.Drawing.Size(116, 23);
            this.upload.TabIndex = 9;
            this.upload.Text = "Upload file vùng";
            this.upload.UseVisualStyleBackColor = true;
            this.upload.Click += new System.EventHandler(this.upload_Click);
            // 
            // web
            // 
            this.web.Location = new System.Drawing.Point(142, 553);
            this.web.Name = "web";
            this.web.Size = new System.Drawing.Size(141, 23);
            this.web.TabIndex = 10;
            this.web.Text = "Upload file âm thanh";
            this.web.UseVisualStyleBackColor = true;
            this.web.Click += new System.EventHandler(this.web_Click);
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(399, 498);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(75, 23);
            this.cancel.TabIndex = 11;
            this.cancel.Text = "Hủy";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // exit
            // 
            this.exit.Location = new System.Drawing.Point(533, 527);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(53, 43);
            this.exit.TabIndex = 12;
            this.exit.Text = "Exit";
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 638);
            this.ControlBox = false;
            this.Controls.Add(this.exit);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.web);
            this.Controls.Add(this.upload);
            this.Controls.Add(this.capture);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.vlcControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.vlcControl1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Vlc.DotNet.Forms.VlcControl vlcControl1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button capture;
        private System.Windows.Forms.Button upload;
        private System.Windows.Forms.Button web;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.Button exit;
    }
}

